USE [DoAnData]
GO
/****** Object:  Table [dbo].[Advertisement]    Script Date: 5/27/2022 4:14:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Advertisement](
	[ID] [int] NOT NULL,
	[Image] [nvarchar](50) NULL,
	[Img_Link] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NULL,
 CONSTRAINT [ID_Advertisement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Auth_asignments]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Auth_asignments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Item_Id] [int] NULL,
	[Item_Name] [nvarchar](50) NULL,
	[User_ID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Auth_item_childs]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Auth_item_childs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Child] [nvarchar](50) NULL,
	[Parent] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Auth_items]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Auth_items](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[Rule_Name] [nvarchar](50) NULL,
	[Data] [nvarchar](50) NULL,
	[Created_at] [datetime] NULL,
	[Update_at] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Slug] [nvarchar](50) NULL,
	[Parent] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[Created_at] [datetime] NULL,
	[Update_at] [datetime] NULL,
 CONSTRAINT [ID] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comments]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Messenger] [nvarchar](50) NULL,
	[Product_ID] [int] NULL,
	[Customer_ID] [int] NULL,
	[Created_at] [datetime] NULL,
	[Parent_ID] [int] NULL,
 CONSTRAINT [ID_Comments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Subject] [nvarchar](50) NULL,
	[Body] [nvarchar](50) NULL,
	[Created_at] [datetime] NULL,
	[Update_at] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NULL,
	[Full_Name] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Phone] [int] NULL,
	[Address] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[Created_at] [datetime] NULL,
	[Update_at] [datetime] NULL,
 CONSTRAINT [ID_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Delivers]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Delivers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[Created_at] [datetime] NULL,
	[Update_at] [datetime] NULL,
 CONSTRAINT [ID_Delivers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Images]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Images](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Pro_Id] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[img_link] [nvarchar](50) NULL,
 CONSTRAINT [ID_Images] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Like_Comments]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Like_Comments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Comment_ID] [int] NULL,
	[Customer_ID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[Id] [int] NOT NULL,
	[Content] [nvarchar](max) NOT NULL,
	[Created_at] [date] NULL,
	[Update_at] [date] NULL,
	[Img] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order_Items]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order_Items](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Order_ID] [int] NULL,
	[Product_ID] [int] NULL,
	[Price] [int] NULL,
	[Return_status] [nvarchar](50) NULL,
	[Quantity] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Customer_ID] [int] NULL,
	[Order_Note] [nvarchar](50) NULL,
	[Amount] [nvarchar](50) NULL,
	[Member_ID] [int] NULL,
	[Username_Ship] [nvarchar](50) NULL,
	[Address_Ship] [nvarchar](50) NULL,
	[Email_Ship] [nvarchar](50) NULL,
	[Phone_Ship] [int] NULL,
	[Shipping_Method] [int] NULL,
	[Payment_Method] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[Created_at] [datetime] NULL,
	[Update_at] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[payment]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[payment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[Created_at] [datetime] NULL,
	[Update_at] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_Colors]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Colors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Product_ID] [int] NULL,
	[Color_ID] [int] NULL,
 CONSTRAINT [ID_Product_Colors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Slug] [nvarchar](50) NULL,
	[Image] [nvarchar](50) NULL,
	[Price] [int] NULL,
	[Sale_Price] [int] NULL,
	[Cat_id] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[Img_Hover] [nvarchar](50) NULL,
	[Type_Pro] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NULL,
	[Created_at] [datetime] NULL,
	[Update_at] [datetime] NULL,
	[CoGiay] [nvarchar](50) NULL,
	[KieuDe] [nvarchar](50) NULL,
	[DoCao] [nvarchar](50) NULL,
	[ChatLieu] [nvarchar](50) NULL,
	[Size] [nvarchar](50) NULL,
	[MaSP] [nvarchar](50) NULL,
	[Quantity] [int] NULL,
 CONSTRAINT [IDd] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shipping_Methods]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shipping_Methods](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Price] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Wishlists]    Script Date: 5/27/2022 4:14:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wishlists](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Cus_ID] [int] NULL,
	[Pro_ID] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[Created_at] [datetime] NULL,
 CONSTRAINT [ID_Wishlists] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Category] ([Id], [Name], [Slug], [Parent], [Status], [Created_at], [Update_at]) VALUES (1, N'BOOT', N'/BOOT', N'Giày', NULL, CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime))
INSERT [dbo].[Category] ([Id], [Name], [Slug], [Parent], [Status], [Created_at], [Update_at]) VALUES (2, N'GB CLASSIC', N'/GB CLASSIC', N'Giày', NULL, CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime))
INSERT [dbo].[Category] ([Id], [Name], [Slug], [Parent], [Status], [Created_at], [Update_at]) VALUES (3, N'GB WARRIORS', N'/GB WARRIORS', N'Giày', NULL, CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Images] ON 

INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (1, 5, NULL, N'GB BOOT B1620_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (2, 5, NULL, N'GB BOOT B1620_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (3, 5, NULL, N'GB BOOT B1620_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (4, 5, NULL, N'GB BOOT B1620_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (5, 5, NULL, N'GB BOOT B1620_5.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (6, 5, NULL, N'GB BOOT B1620_6.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (7, 5, NULL, N'GB BOOT B1620_7.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (8, 5, NULL, N'GB BOOT B1620_8.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (9, 5, NULL, N'GB BOOT B1620_9.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (10, 6, NULL, N'GB BOOT B1634_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (11, 6, NULL, N'GB BOOT B1634_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (12, 6, NULL, N'GB BOOT B1634_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (13, 6, NULL, N'GB BOOT B1634_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (14, 7, NULL, N'GB BOOT B1635_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (15, 7, NULL, N'GB BOOT B1635_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (16, 7, NULL, N'GB BOOT B1635_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (17, 7, NULL, N'GB BOOT B1635_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (18, 7, NULL, N'GB BOOT B1635_5.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (19, 7, NULL, N'GB BOOT B1635_6.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (20, 7, NULL, N'GB BOOT B1635_7.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (21, 7, NULL, N'GB BOOT B1635_8.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (22, 7, NULL, N'GB BOOT B1635_9.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (23, 12, NULL, N'GB CLASSIC B1176_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (24, 12, NULL, N'GB CLASSIC B1176_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (25, 13, NULL, N'GB CLASSIC B1443_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (26, 13, NULL, N'GB CLASSIC B1443_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (27, 13, NULL, N'GB CLASSIC B1443_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (28, 13, NULL, N'GB CLASSIC B1443_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (29, 13, NULL, N'GB CLASSIC B1443_5.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (30, 13, NULL, N'GB CLASSIC B1443_6.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (31, 13, NULL, N'GB CLASSIC B1443_7.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (32, 13, NULL, N'GB CLASSIC B1443_8.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (33, 13, NULL, N'GB CLASSIC B1443_9.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (34, 13, NULL, N'GB CLASSIC B1443_10.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (35, 8, NULL, N'GB CLASSIC B1623_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (36, 8, NULL, N'GB CLASSIC B1623_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (37, 8, NULL, N'GB CLASSIC B1623_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (38, 8, NULL, N'GB CLASSIC B1623_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (39, 9, NULL, N'GB CLASSIC B1624_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (40, 9, NULL, N'GB CLASSIC B1624_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (41, 9, NULL, N'GB CLASSIC B1624_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (42, 9, NULL, N'GB CLASSIC B1624_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (43, 9, NULL, N'GB CLASSIC B1624_5.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (44, 9, NULL, N'GB CLASSIC B1624_6.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (45, 9, NULL, N'GB CLASSIC B1624_7.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (46, 9, NULL, N'GB CLASSIC B1624_8.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (47, 9, NULL, N'GB CLASSIC B1624_9.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (48, 9, NULL, N'GB CLASSIC B1624_10.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (49, 10, NULL, N'GB CLASSIC B1631_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (50, 10, NULL, N'GB CLASSIC B1631_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (51, 10, NULL, N'GB CLASSIC B1631_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (52, 10, NULL, N'GB CLASSIC B1631_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (53, 10, NULL, N'GB CLASSIC B1631_5.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (54, 10, NULL, N'GB CLASSIC B1631_6.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (55, 10, NULL, N'GB CLASSIC B1631_7.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (56, 10, NULL, N'GB CLASSIC B1631_8.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (57, 10, NULL, N'GB CLASSIC B1631_9.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (58, 23, NULL, N'GB CLASSIC B1636_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (59, 23, NULL, N'GB CLASSIC B1636_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (60, 23, NULL, N'GB CLASSIC B1636_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (61, 23, NULL, N'GB CLASSIC B1636_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (62, 23, NULL, N'GB CLASSIC B1636_5.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (63, 23, NULL, N'GB CLASSIC B1636_6.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (64, 23, NULL, N'GB CLASSIC B1636_7.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (65, 23, NULL, N'GB CLASSIC B1636_8.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (66, 14, NULL, N'GB CLASSIC B1637_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (67, 14, NULL, N'GB CLASSIC B1637_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (68, 14, NULL, N'GB CLASSIC B1637_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (69, 14, NULL, N'GB CLASSIC B1637_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (70, 15, NULL, N'GB CLASSIC B1638_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (71, 15, NULL, N'GB CLASSIC B1638_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (72, 15, NULL, N'GB CLASSIC B1638_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (73, 15, NULL, N'GB CLASSIC B1638_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (74, 16, NULL, N'GB CLASSIC B1639_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (75, 16, NULL, N'GB CLASSIC B1639_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (76, 16, NULL, N'GB CLASSIC B1639_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (77, 16, NULL, N'GB CLASSIC B1639_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (78, 17, NULL, N'GB CLASSIC B1641_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (79, 17, NULL, N'GB CLASSIC B1641_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (80, 17, NULL, N'GB CLASSIC B1641_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (81, 17, NULL, N'GB CLASSIC B1641_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (82, 17, NULL, N'GB CLASSIC B1641_5.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (83, 18, NULL, N'GB CLASSIC B1645_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (84, 18, NULL, N'GB CLASSIC B1645_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (85, 18, NULL, N'GB CLASSIC B1645_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (86, 19, NULL, N'GB CLASSIC B1646_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (87, 19, NULL, N'GB CLASSIC B1646_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (88, 19, NULL, N'GB CLASSIC B1646_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (89, 19, NULL, N'GB CLASSIC B1646_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (90, 20, NULL, N'GB CLASSIC B1655_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (91, 20, NULL, N'GB CLASSIC B1655_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (92, 20, NULL, N'GB CLASSIC B1655_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (93, 20, NULL, N'GB CLASSIC B1655_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (94, 24, NULL, N'GB WARRIORS B1622_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (95, 24, NULL, N'GB WARRIORS B1622_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (96, 24, NULL, N'GB WARRIORS B1622_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (97, 24, NULL, N'GB WARRIORS B1622_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (98, 24, NULL, N'GB WARRIORS B1622_5.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (99, 24, NULL, N'GB WARRIORS B1622_6.webp')
GO
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (100, 24, NULL, N'GB WARRIORS B1622_7.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (101, 24, NULL, N'GB WARRIORS B1622_8.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (102, 24, NULL, N'GB WARRIORS B1622_9.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (103, 25, NULL, N'GB WARRIORS B1732_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (104, 25, NULL, N'GB WARRIORS B1732_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (105, 25, NULL, N'GB WARRIORS B1732_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (106, 25, NULL, N'GB WARRIORS B1732_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (107, 25, NULL, N'GB WARRIORS B1732_5.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (108, 25, NULL, N'GB WARRIORS B1732_6.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (109, 25, NULL, N'GB WARRIORS B1732_7.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (110, 25, NULL, N'GB WARRIORS B1732_8.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (111, 26, NULL, N'GB WARRIORS L1628_1.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (112, 26, NULL, N'GB WARRIORS L1628_2.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (113, 26, NULL, N'GB WARRIORS L1628_3.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (114, 26, NULL, N'GB WARRIORS L1628_4.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (115, 26, NULL, N'GB WARRIORS L1628_5.webp')
INSERT [dbo].[Images] ([Id], [Pro_Id], [Status], [img_link]) VALUES (116, 26, NULL, N'GB WARRIORS L1628_6.webp')
SET IDENTITY_INSERT [dbo].[Images] OFF
GO
INSERT [dbo].[News] ([Id], [Content], [Created_at], [Update_at], [Img], [Name]) VALUES (1, N'Khởi động chương trình sale lớn đầu năm Nhâm Dần, màn collab giữa Shopee x giayBOM 15.3.2022 cực khủng. Tung hàng triệu voucher cực xịn - Sale up to lên đến 50%. Cùng BOM tìm hiểu thêm thông tin Siêu Hội Sale - để không bị bỏ lỡ khung giờ vàng nào nhé.  



Đến với chương trình sale 15.3 của giayBOM trên sàn thương mại điện tử Shopee, bạn sẽ được áp dụng 4 ưu đãi cực xịn chỉ có riêng giayBOM. Đơn hàng được áp dụng Voucher giayBOM (tung ra trên từng khung giờ “Vàng" hoặc xem Livestream để lấy mã Voucher). Ngoài ra, bạn có thể áp dụng Voucher trên Shopee trên cùng đơn hàng. Hơn thế nữa bạn được Freeship Extra cho tổng hơn hàng 70K và được tặng xu khi kết thúc đơn hàng luôn nhé. 04 ưu đãi cực hấp dẫn này chỉ có vào ngày 15.3 thôi nhé. Chỉ cần canh sale - Chốt đơn liền tay để nhận sản phẩm chất lượng với giá hạt dẻ. ', CAST(N'2022-01-01' AS Date), CAST(N'2022-02-02' AS Date), N'tintuc1.webp', N'SIÊU HỘI SĂN DEAL - UP TO 50% SALE HẾT SẢY TẠI SHOPEE FASHION SHOES')
INSERT [dbo].[News] ([Id], [Content], [Created_at], [Update_at], [Img], [Name]) VALUES (2, N'Dòng Prime tại giayBOM rất phù hợp cho bạn đến trường khi diện cùng đồng phục. Thiết kế đơn giản và tạo cảm giác thoải mái khi mang.GB Prime Change Color B1643Mẫu thiết kế đổi màu B1643 với diện mạo “đáng iu" cho những cô nàng bánh bèo. Mẫu giày GB Prime Change Color được làm bằng da PU dễ dàng v...', CAST(N'2022-01-01' AS Date), CAST(N'2022-02-02' AS Date), N'tintuc2.webp', N'DIỆN NGAY MẪU MỚI PRIME CÙNG BẠN “BACK TO SCHOOL" SIÊU ĐÁNG YÊU')
INSERT [dbo].[News] ([Id], [Content], [Created_at], [Update_at], [Img], [Name]) VALUES (3, N'Nàng không biết chọn kiểu giày nào có khả năng “hack dáng" nhưng vẫn đảm bảo sự thoải mái trong một ngày dài làm việc và học tập. Nàng đang phân vân không biết chọn lựa mẫu nào thì cùng giayBOM nhé. Mẫu giày GB Classic B1443Các tín đồ nhà Bomer không còn quá xa lạ với mẫu Classic huyền thoại B144...', CAST(N'2022-01-02' AS Date), CAST(N'2022-03-03' AS Date), N'tintuc3.webp', N'04 KIỂU GIÀY CLASSIC GIÚP CÔ NÀNG “NẤM LÙN" CÂN MỌI TRANG PHỤC CÔNG SỞ')
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (5, N'GB BOOT B1620', N'/GB BOOT B1620', N'GB BOOT B1620_1.webp', 100, 360, 1, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N'Cổ cao có lưỡi gà rời đệm chân', N'Đế đúc cao su non', N'~ 5 cm', N' Da PU tổng hợp', N'35 - 36 - 37 - 38 - 39 - 40', N'B1620', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (6, N'GB BOOT B1634', N'/GB BOOT B1634', N'GB BOOT B1634_2.webp', 100, 360, 1, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N'Cổ cao có lưỡi gà rời đệm chân', N'Đế đúc cao su non', N'~ 4 cm', N'Da PU tổng hợp ', N'35 - 36 - 37 - 38 - 39', N'B1634', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (7, N'GB BOOT B1635', N'/GB BOOT B1635', N'GB BOOT B1635_1.webp', 100, 360, 1, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N'Cổ cao có lưỡi gà rời đệm chân', N'Đế đúc cao su non', N' ~ 4 cm', N'Da PU tổng hợp', N'35 - 36 - 37 - 38 - 39', N'B1635', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (8, N'GB CLASSIC B1623', N'/GB CLASSIC B1623', N'GB CLASSIC B1623_1.webp', 100, 360, 2, N'mới', NULL, NULL, N'0', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non có rãnh tạo ma sát chống trượt', N' ~5 cm', N'Da Pu tổng hợp', N' 35 - 36 - 37 - 38 - 39 - 40', N'B1623', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (9, N'GB CLASSIC B1624', N'/GB CLASSIC B1624', N'GB CLASSIC B1624_2.webp', 100, 360, 2, N'mới', NULL, NULL, N'0', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non có rãnh tạo ma sát chống trượt', N' ~5 cm', N'Da Pu tổng hợp', N' 35 - 36 - 37 - 38 - 39 - 40', N'B1624', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (10, N'GB CLASSIC B1631', N'/GB CLASSIC B1631', N'GB CLASSIC B1631_2.webp', 100, 360, 2, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non có rãnh tạo ma sát chống trượt', N' ~5 cm', N'Da Pu tổng hợp', N' 35 - 36 - 37 - 38 - 39 - 40', N'B1631', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (11, N'GB CLASSIC B1639', N'/GB CLASSIC B1639', N'GB CLASSIC B1639_1.webp', 100, 360, 2, N'mới', NULL, NULL, N'0', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non có rãnh tạo ma sát chống trượt', N' ~5 cm', N'Da Pu tổng hợp', N' 35 - 36 - 37 - 38 - 39 - 40', N'B1639', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (12, N'GB CLASSIC B1176', N'/GB CLASSIC B1179', N'GB CLASSIC B1176_1.webp', 100, 360, 2, N'mới', NULL, NULL, N'0', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non có rãnh tạo ma sát chống trượt', N' ~5 cm', N'Da Pu tổng hợp', N' 35 - 36 - 37 - 38 - 39 - 40', N'B1176', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (13, N'GB CLASSIC B1443', N'/GB CLASSIC B1443', N'GB CLASSIC B1443_1.webp', 100, 360, 2, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non có rãnh tạo ma sát chống trượt', N' ~5 cm', N'Da Pu tổng hợp', N' 35 - 36 - 37 - 38 - 39 - 40', N'B1443', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (14, N'GB CLASSIC B1637', N'/GB CLASSIC B1637', N'GB CLASSIC B1637_1.webp', 100, 360, 2, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non', N' ~ 3 cm', N'Da PU', N'35 - 36 - 37 - 38 - 39', N'B1637', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (15, N'GB CLASSIC B1638', N'/GB CLASSIC B1638', N'GB CLASSIC B1638_1.webp', 100, 360, 1, N'mới', NULL, NULL, N'0', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non', N'~ 3 cm', N' Da PU', N'35 - 36 - 37 - 38 - 39 - 40', N'B1638', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (16, N'GB CLASSIC B1639', N'/GB CLASSIC B1639', N'GB CLASSIC B1639_1.webp', 100, 360, 1, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non', N'~ 3.5 cm', N' Da PU ', N'35 - 36 - 37 - 38 - 39 - 40', N'B1639', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (17, N'GB CLASSIC B1641', N'/GB CLASSIC B1641', N'GB CLASSIC B1641_1.webp', 100, 360, 1, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non', N'~ 3.5 cm', N' Da PU ', N'35 - 36 - 37 - 38 - 39 - 40', N'B1641', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (18, N'GB CLASSIC B1645', N'/GB CLASSIC B1645', N'GB CLASSIC B1645_1.webp', 100, 360, 1, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non', N'~ 3.5 cm', N' Da PU ', N'35 - 36 - 37 - 38 - 39 - 40', N'B1645', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (19, N'GB CLASSIC B1646', N'/GB CLASSIC B1646', N'GB CLASSIC B1646_1.webp', 100, 360, 1, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non', N'~ 3.5 cm', N' Da PU ', N'35 - 36 - 37 - 38 - 39 - 40', N'B1646', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (20, N'GB CLASSIC B1655', N'/GB CLASSIC B1655', N'GB CLASSIC B1655_1.webp', 100, 360, 1, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N' Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non', N'~ 3.5 cm', N' Da PU ', N'35 - 36 - 37 - 38 - 39 - 40', N'B1655', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (21, N'GB BOOT B1620', N'/GB BOOT B1620', N'GB BOOT B1620_1.webp', 100, 360, 1, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N'Cổ cao có lưỡi gà rời đệm chân', N'Đế đúc cao su non', N'~ 5 cm', N' Da PU tổng hợp', N'35 - 36 - 37 - 38 - 39 - 40', N'B1620', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (22, N'GB BOOT B1620', N'/GB BOOT B1620', N'GB BOOT B1620_1.webp', 100, 360, 1, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N'Cổ cao có lưỡi gà rời đệm chân', N'Đế đúc cao su non', N'~ 5 cm', N' Da PU tổng hợp', N'35 - 36 - 37 - 38 - 39 - 40', N'B1620', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (23, N'GB CLASSIC B1636', N'/GB CLASSIC B1636', N'GB CLASSIC B1636_1.webp', 100, 300, 2, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N'Loại cổ thấp có lưỡi gà đệm chân', N'Đế đúc cao su non có rãnh tạo ma sát chống trượt', N'~ 3 cm', N' Da PU', N'35 - 36 - 37 - 38 - 39 - 40', N'B1636', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (24, N'GB WARRIORS B1622', N'/GB WARRIORS B1622', N'GB WARRIORS B1622_1.webp', 100, 300, 3, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N'Loại cổ thấp ', N'Đế đúc cao su non có rãnh tạo ma sát chống trượt', N'~ 5 cm', N'  Da PU', N'35 - 36 - 37 - 38 - 39 - 40', N'B1622', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (25, N'GB WARRIORS B1732', N'/GB WARRIORS B1732', N'GB WARRIORS B1732_1.webp', 100, 300, 3, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N'Cổ cao có lưỡi gà rời đệm chân', N'Đế đúc cao su non có rãnh tạo ma sát chống trượt', N'~ 5 cm', N'  Da PU', N'35 - 36 - 37 - 38 - 39 - 40', N'B1732', 100)
INSERT [dbo].[Products] ([Id], [Name], [Slug], [Image], [Price], [Sale_Price], [Cat_id], [Status], [Img_Hover], [Type_Pro], [Type], [Created_at], [Update_at], [CoGiay], [KieuDe], [DoCao], [ChatLieu], [Size], [MaSP], [Quantity]) VALUES (26, N'GB WARRIORS L1628', N'/GB WARRIORS L1628', N'GB WARRIORS L1628_1.webp', 100, 300, 3, N'mới', NULL, NULL, N'1', CAST(N'2022-01-01T00:00:00.000' AS DateTime), CAST(N'2022-01-01T00:00:00.000' AS DateTime), N'Cổ cao có lưỡi gà rời đệm chân', N'Đế đúc cao su non có rãnh tạo ma sát chống trượt', N'~ 5 cm', N'  Da PU', N'35 - 36 - 37 - 38 - 39 - 40', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
ALTER TABLE [dbo].[Product_Colors]  WITH CHECK ADD FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Products] ([Id])
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD FOREIGN KEY([Cat_id])
REFERENCES [dbo].[Category] ([Id])
GO
ALTER TABLE [dbo].[Wishlists]  WITH CHECK ADD FOREIGN KEY([Cus_ID])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Wishlists]  WITH CHECK ADD FOREIGN KEY([Pro_ID])
REFERENCES [dbo].[Products] ([Id])
GO
