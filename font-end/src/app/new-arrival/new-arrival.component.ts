import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-new-arrival',
  templateUrl: './new-arrival.component.html',
  styleUrls: ['./new-arrival.component.css'],
})
export class NewArrivalComponent implements OnInit {
  constructor(private service: SharedService) {}

  NewProduct: any = [];

  ngOnInit(): void {
    this.NewPro();
  }
  NewPro() {
    this.service.GetNewArrival().subscribe((data) => {
      this.NewProduct = data;
    });
  }
  Sortascending() {
    this.NewProduct = this.NewProduct.sort((a: any, b: any) =>
      a.Sale_Price > b.Sale_Price ? 1 : b.Sale_Price > a.Sale_Price ? -1 : 0
    );
    console.log(this.NewProduct);
  }
  Sortdecreasing() {
    this.NewProduct = this.NewProduct.sort(
      (a: any, b: any) => b.Sale_Price - a.Sale_Price
    );
  }
}
