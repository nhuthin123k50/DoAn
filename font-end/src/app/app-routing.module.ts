import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BestSellerComponent } from './best-seller/best-seller.component';
import { CartComponent } from './cart/cart.component';
import { HomeComponent } from './home/home.component';
import { ListProductComponent } from './list-product/list-product.component';
import { LoginComponent } from './login/login.component';
import { NewArrivalComponent } from './new-arrival/new-arrival.component';
import { PrimeComponent } from './prime/prime.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductComponent } from './product/product.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'product',
    component: ProductComponent,
  },
  {
    path: 'new-arrival',
    component: NewArrivalComponent,
  },
  {
    path: 'best-seller',
    component: BestSellerComponent,
  },
  {
    path: 'product-detail/:id',
    component: ProductDetailComponent,
  },

  {
    path: 'list-product/:id',
    component: ListProductComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'prime',
    component: PrimeComponent,
  },
  {
    path: 'cart',
    component: CartComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
