import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(
    private service: SharedService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  ProductList: any = [];
  Category: any = [];
  boots: any = [];
  listNews: any = [];
  ListCategory: any = [];
  NewProduct: any = [];
  size = 12;
  sizecate = 3;
  ngOnInit(): void {
    this.DSSP();
    this.DS();
    this.ListCate();
    this.OnClick;
    this.NewPro();
    this.GetBoot();
    this.ListNews();
  }
  DSSP() {
    this.service.GetListBestSeller().subscribe((data) => {
      this.ListCategory = data.slice(0, this.size);
    });
  }

  DS() {
    this.service.listCategory().subscribe((data) => {
      this.Category = data.slice(0, this.sizecate);
    });
  }
  ListCate() {
    this.service.listCategoryProduct().subscribe((data) => {
      this.ListCategory = data.slice(0, this.size);
    });
  }

  OnClick(IDPro: any) {
    this.service.GetListPro(IDPro).subscribe((data) => {
      this.ListCategory = data.slice(0, this.size);
    });
  }
  NewPro() {
    this.service.GetNewArrival().subscribe((data) => {
      this.NewProduct = data.slice(0, this.size);
    });
  }
  GetBoot() {
    this.service.GetBoots().subscribe((data) => {
      this.boots = data.slice(0, this.size);
    });
  }
  ListNews() {
    this.service.listNews().subscribe((data) => {
      this.listNews = data;
    });
  }
}
