import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';
import { MatDialog } from '@angular/material/dialog';
import { PopupThanhToanComponent } from './popup-thanh-toan/popup-thanh-toan.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  Quantity: number = 1;
  animal: any;
  name: any;
  i = 1;
  sum = 0;
  Sum = 0;
  sumbill = 0;
  tax = 0;
  ListCartPro: any = [];
  constructor(private service: SharedService, public dialogRef: MatDialog) {}
  openDialog(): void {
    const dialogRef = this.dialogRef.open(PopupThanhToanComponent);
  }
  ngOnInit(): void {
    this.ListCartPro = JSON.parse(localStorage.getItem('listProd') || '[]');
    console.log(this.ListCartPro);
    this.thanhTien();
  }
  thanhTien() {
    this.sum = 0;
    for (let index = 0; index < this.ListCartPro.length; index++) {
      this.sum += this.ListCartPro[index].Sum;
    }
    this.tax = 0.1 * this.sum;
    this.sumbill = this.tax + this.sum;
  }
  removeItem(IdPro: any, Color: any, Size: any) {
    console.log(this.ListCartPro);
    if (this.ListCartPro.length > 1) {
      const a = this.ListCartPro.findIndex(
        (x: any) => x.IdProd === IdPro && x.Color === Color && x.Size === Size
      );

      this.ListCartPro.splice(a, 1);
    } else {
      this.ListCartPro = [];
    }

    localStorage.setItem('listProd', JSON.stringify(this.ListCartPro));
    this.thanhTien();
    // this.ListCartPro = this.ListCartPro.filter(
    //   (x: any) => x.IdProd !== IdPro && x.Color !== Color && x.Size !== Size
    // );
  }
  minus(item: any) {
    item.Quantity--;
    item.Sum = item.Quantity * item.Price;
    this.thanhTien();
    item.Sum.push(this.ListCartPro.Sum);
    localStorage.setItem('listProd', JSON.stringify(this.ListCartPro));
  }
  plus(item: any) {
    item.Quantity++;
    item.Sum = item.Quantity * item.Price;
    this.thanhTien();
    localStorage.setItem('listProd', JSON.stringify(this.ListCartPro));
  }
}
