import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SharedService } from './shared.service';
import { HttpClientModule } from '@angular/common/http';
import { CategoryProductComponent } from './category-product/category-product.component';
import { BestSellerComponent } from './best-seller/best-seller.component';
import { NewArrivalComponent } from './new-arrival/new-arrival.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { FormsModule } from '@angular/forms';
import { SidebarModule } from 'ng-sidebar';
import { ListProductComponent } from './list-product/list-product.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PrimeComponent } from './prime/prime.component';
import { CartComponent } from './cart/cart.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { PopupThanhToanComponent } from './cart/popup-thanh-toan/popup-thanh-toan.component';
import { LoginComponent } from './login/login.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CategoryProductComponent,
    BestSellerComponent,
    NewArrivalComponent,
    ProductDetailComponent,
    ListProductComponent,
    HeaderComponent,
    FooterComponent,
    PrimeComponent,
    CartComponent,
    LoginComponent,
    PopupThanhToanComponent,
  ],
  imports: [
    AppRoutingModule,
    CommonModule,
    MatDialogModule,
    BrowserModule,
    HttpClientModule,
    SidebarModule.forRoot(),

    FormsModule,

    BrowserAnimationsModule,
  ],
  providers: [SharedService, HttpClient],
  bootstrap: [AppComponent],
})
export class AppModule {}
