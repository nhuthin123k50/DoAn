import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css'],
})
export class ListProductComponent implements OnInit {
  listCate: any = '';
  ListProduct: any = [];
  constructor(
    private service: SharedService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.params.subscribe((params) => {
      this.paramsChange(params['id']);
    });
  }

  ngOnInit(): void {
    const idProd = this.route.snapshot.paramMap.get('id');
    this.ListPro(idProd);
    this.paramsChange;
    this.Getcate(idProd);
  }
  ListPro(IDPro: any) {
    this.service.GetListPro(IDPro).subscribe((data) => {
      this.ListProduct = data;
    });
  }
  paramsChange(id: any) {
    this.ListPro(id);
    this.Getcate(id);
  }
  Getcate(id: any) {
    this.service.listCategory().subscribe((data) => {
      this.listCate = data.find((x) => x.Id === parseInt(id));
    });
  }
}
