import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';
@Component({
  selector: 'app-category-product',
  templateUrl: './category-product.component.html',
  styleUrls: ['./category-product.component.css'],
})
export class CategoryProductComponent implements OnInit {
  constructor(private service: SharedService) {}
  listBestSeller: any = [];

  ngOnInit(): void {
    this.ListBestSeller();
  }

  ListBestSeller() {
    this.service.GetListBestSeller().subscribe((data) => {
      this.listBestSeller = data;
    });
  }
}
