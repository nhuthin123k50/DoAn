import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  listCategory: any = [];
  seach: any = [];
  text: string = '';
  keySearch: string = '';
  ListCartPro: any = [];
  count: any;
  constructor(private service: SharedService) {}

  ngOnInit(): void {
    this.ListCate();
    this.Count();
  }
  Count() {
    this.ListCartPro = JSON.parse(localStorage.getItem('listProd') || '[]');

    this.count = this.ListCartPro.length;
  }
  ListCate() {
    this.service.GetListCate().subscribe((data) => {
      this.listCategory = data;
    });
  }
  Seach(a: any) {
    if (a) {
      this.service.seach(a).subscribe((data) => {
        this.seach = data;
      });
    } else {
      this.seach = [];
    }
  }
  onKeyUp(Event: any) {
    this.Seach(this.keySearch);
    // appending the updated value to the variable
  }
  DSSP() {
    this.service.GetListBestSeller().subscribe((data) => {
      this.listCategory = data;
    });
  }
  OnClick(IDPro: any) {
    this.service.GetListPro(IDPro).subscribe((data) => {
      this.listCategory = data;
    });
  }
}
