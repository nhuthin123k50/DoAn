import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  readonly APIurl = 'http://localhost:5184/api';
  readonly Photourl = 'http://localhost:5184/Photos';
  constructor(private http: HttpClient) {}
  //api product
  listProduct(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Product');
  }
  AddProduct(val: any) {
    return this.http.get<any>(this.APIurl + '/Product', val);
  }
  EditProduct(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Product');
  }
  DeleteProduct(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Product');
  }
  listCategoryProduct(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Product');
  }

  GetListPro(ID: any): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Product/' + ID);
  }
  //api color
  listNews(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/News');
  }
  AddNews(val: any) {
    return this.http.get<any>(this.APIurl + '/News', val);
  }
  EditNews(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/News');
  }
  DeleteNews(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/News');
  }
  //api orders
  listOrders(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Orders');
  }
  AddOrders(val: any) {
    return this.http.get<any>(this.APIurl + '/Orders', val);
  }
  EditOrders(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Orders');
  }
  DeleteOrders(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Orders');
  }
  listCategory(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Category');
  }
  AddCategory(val: any) {
    return this.http.get<any>(this.APIurl + '/Category', val);
  }
  GetListCate(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Category/list-category');
  }
  EditCategory(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Category');
  }
  DeleteCategory(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Category');
  }
  GetListBestSeller(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Product/best-seller');
  }
  GetNewArrival(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Product/new-arrival');
  }
  GetProductDetails(ID: any): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Product/product-details/' + ID);
  }
  GetBoots(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Product/boots');
  }
  listImage(Pro_Id: any): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Images/' + Pro_Id);
  }
  AddImage(val: any) {
    return this.http.get<any>(this.APIurl + '/Images', val);
  }
  EditImage(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Images');
  }
  DeleteImage(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Images');
  }
  GetAccount(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Account');
  }
  GetPrime(): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Product/Prime');
  }
  seach(a: any): Observable<any[]> {
    return this.http.get<any>(this.APIurl + '/Product/seach/?a=' + a);
  }
}
