import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-best-seller',
  templateUrl: './best-seller.component.html',
  styleUrls: ['./best-seller.component.css'],
})
export class BestSellerComponent implements OnInit {
  constructor(private service: SharedService) {}
  listBestSeller: any = [];

  ngOnInit(): void {
    this.ListBestSeller();
  }

  ListBestSeller() {
    this.service.GetListBestSeller().subscribe((data) => {
      this.listBestSeller = data;
    });
  }
}
