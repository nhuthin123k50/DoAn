import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SharedService } from '../shared.service';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
})
export class ProductDetailComponent implements OnInit {
  ListCategory: any = [];
  product: any;
  image: any = [];
  name: any = [];
  idProd = '';
  inputnumber = 0;
  Quantity: number = 1;
  i = 1;
  Sum: any;
  changeColor: any;
  size: any;
  color: any;
  saveSize: any;
  saveColor: any;
  toggle = true;
  status = 'Enable';
  checkSize = false;

  constructor(
    private service: SharedService,
    private route: ActivatedRoute,
    public dialogRef: MatDialog
  ) {}

  ngOnInit(): void {
    const idProd = this.route.snapshot.paramMap.get('id');
    this.ListCate(idProd);
    this.GetImg(idProd);
    this.Size(idProd);
  }
  ListCate(ID: any) {
    this.service.GetProductDetails(ID).subscribe((data) => {
      this.ListCategory = data[0];
    });
  }
  minus() {
    if (this.i != 1) this.i--;
    this.Quantity = this.i;
  }
  plus() {
    if (this.i != 20) this.i++;
    this.Quantity = this.i;
  }
  GetImg(Pro_Id: any) {
    this.service.listImage(Pro_Id).subscribe((data) => {
      this.image = data;
    });
  }
  currentDiv(n: any) {
    this.showDivs(n);
  }

  showDivs(n: any) {
    var slideIndex = n;
    var i;
    var x = document.getElementsByClassName(
      'mySlides'
    ) as HTMLCollectionOf<HTMLElement>;
    var dots = document.getElementsByClassName(
      'demo'
    ) as HTMLCollectionOf<HTMLElement>;
    if (n > x.length) {
      slideIndex = 1;
    }
    if (n < 1) {
      slideIndex = x.length;
    }
    for (i = 0; i < x.length; i++) {
      x[i].style.display = 'none';
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(' w3-opacity-off', '');
    }
    x[slideIndex - 1].style.display = 'block';
    dots[slideIndex - 1].className += ' w3-opacity-off';
  }
  Size(ID: any) {
    this.service.GetProductDetails(ID).subscribe((data) => {
      this.ListCategory = data[0];
      this.size = this.ListCategory.Size.split('-');
      this.color = this.ListCategory.Color.split('-');
    });
  }

  addToCart() {
    const obj = {
      IdProd: this.ListCategory.Id,
      Color: this.saveColor,
      Quantity: this.Quantity,
      Size: this.saveSize,
      Price: this.ListCategory.Sale_Price,
      Name: this.ListCategory.Name,
      Image: this.ListCategory.Image,
      Sum: this.Quantity * this.ListCategory.Sale_Price,
    };
    const a = JSON.parse(localStorage.getItem('listProd') || '[]');
    console.log(a);

    var b = a.findIndex(
      (x: any) =>
        x.IdProd === this.ListCategory.Id &&
        x.Size === this.saveSize &&
        x.Color === this.saveColor
    );
    if (b >= 0) {
      a[b].Quantity += this.Quantity;
    } else {
      a.push(obj);
    }
    localStorage.setItem('listProd', JSON.stringify(a));
    Swal.fire(
      'Good job!',
      'sản phẩm của bạn đã được thêm vào giỏ hàng!',
      'success'
    );
  }
  selectColor(item: any) {
    this.saveColor = item;
  }
  selectSize(item: any) {
    this.saveSize = item;
    this.changeColor = 'black';
  }
}
