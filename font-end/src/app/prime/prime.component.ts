import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-prime',
  templateUrl: './prime.component.html',
  styleUrls: ['./prime.component.css'],
})
export class PrimeComponent implements OnInit {
  prime: any = [];
  constructor(private service: SharedService) {}

  ngOnInit(): void {
    this.GetPrime();
  }
  GetPrime() {
    this.service.GetPrime().subscribe((data) => {
      this.prime = data;
    });
  }
}
