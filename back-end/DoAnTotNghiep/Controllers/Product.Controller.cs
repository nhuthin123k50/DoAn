﻿using DoAnTotNghiep.Model;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;


namespace DoAnTotNghiep.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IConfiguration _Configuration;
        public ProductController(IConfiguration configuration)
        {
            _Configuration = configuration;
        }
        [HttpGet]
        public JsonResult Get()
        {
            string query = "select Id ,Name,Slug,Image,Price,Sale_Price,Cat_id,Status,Img_Hover,Type_Pro,Type,Created_at,Update_at ,CoGiay,KieuDe, DoCao,ChatLieu,Size,MaSP,Quantity ,Color from Products";
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult(table);
        }
        [HttpPost]
        public JsonResult Post(Product product)
        {
            string query = @"Insert into Product Values (N'" + product.Name + "')";
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult("Thêm mới thành công");
        }
        [HttpPut]
        public JsonResult Put(Product Product)
        {
            string query = @"Update Product set name= N'" + Product.Name + "'"
                + " where Id= " + Product.Id;
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult("cập nhật thành công");
        }
        [HttpDelete("{ma}")]
        public JsonResult Delete(int ma)
        {
            string query = @"Delete from Product"
                            + " where Id= " + ma;
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult("xóa  thành công");
        }
        [HttpGet("{ID}")]
        public JsonResult GetListPro(int ID)
        {
            string query = "select Id ,Name,Slug,Image,Price,Sale_Price,Cat_id,Status,Img_Hover,Type_Pro,Type,Created_at,Update_at ,CoGiay,KieuDe, DoCao,ChatLieu,Size,MaSP,Quantity,Color  from Products" + " where Cat_id= " + ID;
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult(table);
        }
        [HttpGet("best-seller")]
        public JsonResult GetListBestSeller()
        {
            string query = "select Id ,Name,Slug,Image,Price,Sale_Price,Cat_id,Status,Img_Hover,Type_Pro,Type,Created_at,Update_at ,CoGiay,KieuDe, DoCao,ChatLieu,Size,MaSP,Quantity,Color  from Products where Type= 1";
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult(table);
        }
        [HttpGet("new-arrival")]
        public JsonResult GetNewProduct()
        {
            string query = "select Id ,Name,Slug,Image,Price,Sale_Price,Cat_id,Status,Img_Hover,Type_Pro,Type,Created_at,Update_at ,CoGiay,KieuDe, DoCao,ChatLieu,Size,MaSP,Quantity,Color  from Products where year(Created_at)= 2022";
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult(table);
        }
        [HttpGet("product-details/{ID}")]
        public JsonResult GetProductDetails(int ID)
        {
            string query = "select Id ,Name,Slug,Image,Price,Sale_Price,Cat_id,Status,Img_Hover,Type_Pro,Type,Created_at,Update_at ,CoGiay,KieuDe, DoCao,ChatLieu,Size,MaSP,Quantity,Color  from Products" + " where Id= " + ID;
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult(table);
        }
        [HttpGet("boots")]
        public JsonResult GetBoots()
        {
            string query = "select Id ,Name,Slug,Image,Price,Sale_Price,Cat_id,Status,Img_Hover,Type_Pro,Type,Created_at,Update_at ,CoGiay,KieuDe, DoCao,ChatLieu,Size,MaSP,Quantity,Color  from Products where Cat_id= 1";
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult(table);
        }
        [HttpGet("prime")]
        public JsonResult GetPrime()
        {
            string query = "select Id ,Name,Slug,Image,Price,Sale_Price,Cat_id,Status,Img_Hover,Type_Pro,Type,Created_at,Update_at ,CoGiay,KieuDe, DoCao,ChatLieu,Size,MaSP,Quantity,Color  from Products where Cat_id= 4";
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult(table);
        }
        [HttpGet("seach")]
        public JsonResult GetSeach(string a)
        {
            string query = "select * from Products where Name LIKE '%" + a.Trim() + "%'";
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult(table);
        }




    }
}
