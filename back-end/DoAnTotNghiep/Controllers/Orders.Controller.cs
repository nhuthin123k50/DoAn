﻿using DoAnTotNghiep.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;


namespace DoAnTotNghiep.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IConfiguration _Configuration;
        public OrdersController(IConfiguration configuration)
        {
            _Configuration = configuration;
        }
        [HttpGet]
        public JsonResult Get()
        {
            string query = "select ID ,Customer_ID,Order_Note,Amount,Member_ID,Username_Ship,Address_Ship,Email_Ship,Phone_Ship,Shipping_Method,Payment_Method,Status,Created_at,Update_at from Orders";
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult(table);
        }
        [HttpPost]
        public JsonResult Post(Orders Orders)
        {
            string query = @"Insert into Orders Values (N'" + Orders.Customer_ID + "')";
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult("Thêm mới thành công");
        }
        [HttpPut]
        public JsonResult Put(Orders Orders)
        {
            string query = @"Update Orders set name= N'" + Orders.Customer_ID + "'"
                + " where ID= " + Orders.Id;
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult("cập nhật thành công");
        }
        [HttpDelete("{ma}")]
        public JsonResult Delete(int ma)
        {
            string query = @"Delete from Orders"
                            + " where ID= " + ma; DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult("xóa  thành công");
        }



    }
}
