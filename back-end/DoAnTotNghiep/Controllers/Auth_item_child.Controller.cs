﻿using DoAnTotNghiep.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;


namespace DoAnTotNghiep.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Auth_item_childController : ControllerBase
    {
        private readonly IConfiguration _Configuration;
        public Auth_item_childController(IConfiguration configuration)
        {
            _Configuration = configuration;
        }
        [HttpGet]
        public JsonResult Get()
        {
            string query = "select ID ,Name,Mamau_Auth_item_child,Status,Created_at from Auth_item_childs";
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult(table);
        }
        [HttpPost]
        public JsonResult Post(Auth_item_child Auth_item_child)
        {
            string query = @"Insert into Auth_item_child Values (N'" + Auth_item_child.Child + "')";
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult("Thêm mới thành công");
        }
        [HttpPut]
        public JsonResult Put(Auth_item_child Auth_item_child)
        {
            string query = @"Update Auth_item_child set name= N'" + Auth_item_child.Child + "'"
                + " where ID= " + Auth_item_child.Id;
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult("cập nhật thành công");
        }
        [HttpDelete("{ma}")]
        public JsonResult Delete(int ma)
        {
            string query = @"Delete from Auth_item_child"
                            + " where ID= " + ma;
            DataTable table = new DataTable();
            string sqlDataSource = _Configuration.GetConnectionString("DoAnData");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                    myReader = myCommand.ExecuteReader();
                table.Load(myReader);
                myReader.Close();
                myCon.Close();
            }
            return new JsonResult("xóa  thành công");
        }



    }
}
