﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAnTotNghiep.Model
{
    [Table("Wishlists")]
    public class Wishlist 
    {
        [Key]
        public int Id { get; set; }
        public int   Cus_ID { get; set; }
        public int Pro_ID { get; set; }
        public string Status { get; set; }
        public DateTime Created_at { get; set; }
    }
}
