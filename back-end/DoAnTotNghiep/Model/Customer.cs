﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAnTotNghiep.Model
{
    
    public class Customer 
    {
        [Key]
        public int Id { get; set; }
        public string Username { get; set; }
        public string Full_Name { get; set; }
        public string Email { get; set; }
        [StringLength(10)]
        public int Phone { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime Update_at { get; set; }
            

    }
}
