﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DoAnTotNghiep.Model
{
    [Table("Category")]
    public class Category 
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
       // "hiển thị trên url"
        public string Slug { get; set; }
       //cha con

        public string Parent { get; set; }
        public string Status { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime Update_at { get; set; }

    }
}
