﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAnTotNghiep.Model
{
    [Table("Orders")]
    public class Orders
    {
        [Key]
        public int Id { get; set; }
        public int Customer_ID { get; set; }
        public string Order_Note { get; set; }
        public string Amount { get; set; }
        public int Member_ID { get; set; }
        public string Username_Ship { get; set; }
        public string Address_Ship { get; set; }
        public string Email_Ship { get; set; }
        public int Phone_Ship { get; set; }
        public int Shipping_Method { get; set; }
        public int Payment_Method { get; set; }
        public string Status { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime Update_at { get; set; }


    }
}
