﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAnTotNghiep.Model
{
    [Table("Auth_asignments")]
    public class Auth_asignment
    {
        [Key]
        public int ID { get; set; }
        public int Item_Id { get; set; }
        public string Item_Name { get; set; }
      
        public int User_ID { get; set; }
    }
}
