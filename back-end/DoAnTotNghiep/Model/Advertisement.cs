﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace DoAnTotNghiep.Model
{
    [Table("Advertisements")]
    public class Advertisement 
    {
        [Key]
        public int ID { get; set; }
        public string Image { get; set; }
        public string Img_Link { get; set; }
        public string Type { get; set; }
    }
}
