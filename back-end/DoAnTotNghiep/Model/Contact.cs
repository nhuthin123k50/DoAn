﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAnTotNghiep.Model
{
    [Table("Contacts")]
    public class Contact 
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

            // tieu đề phản hồi
       
        public string Subject { get; set; }
        // nội dung phản hồi
        public string Body { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime Update_at { get; set; }

    }
}
