﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAnTotNghiep.Model
{
    [Table("Like_Comments")]
    public class Like_Comment
    {
        [Key]
        public int Id { get; set; }
        public int  Comment_ID { get; set; }
        public int Customer_ID  { get; set; }
    }
}
