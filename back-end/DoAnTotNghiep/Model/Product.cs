﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAnTotNghiep.Model
{
    [Table("Products")]
    public class Product 
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Image { get; set; }
        public string Content { get; set; }
        public int  Price { get; set; }
        public int Sale_Price { get; set; }
        public int Cat_id { get; set; }
        public string Status { get; set; }
        public string Img_Hover { get; set; }
        public string Type_Pro { get; set; }
        public string Type { get; set; }
        public DateTime Created_at { get; set; }
        public DateTime Update_at { get; set; }
        public string CoGiay { get; set; }
        public string KieuDe { get; set; }
        public string DoCao { get; set; }
        public string ChatLieu { get; set; }
        public string Size { get; set; }
        public string MaSP { get; set; }
        public int Quantity { get; set; }






    }
}
