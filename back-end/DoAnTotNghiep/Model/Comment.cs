﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAnTotNghiep.Model
{
    [Table("Comments")]
    public class Comment 
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Messenger { get; set; }
        public int Product_ID { get; set; }
        public int Customer_ID { get; set; }
        public DateTime Created_at { get; set; }
        public int Parent_ID { get; set; }


    }
}
