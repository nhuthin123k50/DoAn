﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAnTotNghiep.Model
{
    [Table("Imagess")]
    public class Images 
    {
        [Key]
        public int Id { get; set; }
        public int Pro_id { get; set; }
        public string Status { get; set; }
        public string img_link { get; set; }

    }
}
