﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAnTotNghiep.Model
{
    [Table("Auth_item_childs")]
    public class Auth_item_child 
    {
        [Key]
        public int Id { get; set; }
       
        public string Child { get; set; }
        
        public string Parent { get; set; }

    }
}
